'use strict';

/**
 * @ngdoc overview
 * @name networkhubApp
 * @description
 * # networkhubApp
 *
 * Main module of the application.
 */
var networkhubApp = angular
    .module('networkhubApp', [
        'ngAnimate',
        'ngCookies',
        'ngMessages',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ui.bootstrap',
        'angularModalService',
        'angucomplete-alt',
        'angular.filter',
        'angularUtils.directives.dirPagination',
        'file-model'
    ])
    .config(function ($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'views/main.html',
                controller: 'MainCtrl',
                controllerAs: 'main'
            })
            .when('/countries', {
                templateUrl: 'views/countries.html',
                controller: 'CountriesCtrl',
                controllerAs: 'countries'
            })


            .when('/country/:slug', {
                templateUrl: 'views/country.html',
                controller: 'SearchCtrl',
                controllerAs: 'search'
            })
            .when('/spaces/:id', {
                templateUrl: 'views/space.html',
                controller: 'SpaceCtrl',
                controllerAs: 'space'
            })
            .when('/searchresult/:slug', {

                templateUrl: 'views/search.html',
                controller: 'SearchCtrl',
                controllerAs: 'search'
            })
            .otherwise({
                redirectTo: '/'
            });
    });

/**************************************************************************
 * Make environment available in Angular
 *************************************************************************/

networkhubApp.constant('__env', __env);

function logEnvironment($log, __env) {
    $log.debug('Environment variables:');
    $log.debug(__env)
}

logEnvironment.$inject = ['$log', '__env'];

networkhubApp.run(logEnvironment);
  
