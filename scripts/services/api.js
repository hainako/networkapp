'use strict';

/**
 * @ngdoc service
 * @name networkhubApp.API
 * @description
 * # API
 * Factory in the networkhubApp.
 */
angular.module('networkhubApp')
    .factory('API', ['$http', function ($http) {

        var urlBase = 'https://test.thenetworkhub.net/cvm/webservices/listings/';
        var dataFactory = {};

        dataFactory.getSpaces = function () {
            return $http.get(urlBase + 'getAll');
        };

        dataFactory.getCountries = function () {
            return $http.get(urlBase + 'countryList');
        };

        dataFactory.getCountrySpaces = function (slug) {
            return $http.get(urlBase + 'getListingByCountry/' + slug);
        };

        dataFactory.getSpace = function (id) {
            return $http.get(urlBase + 'getRoomDetail/' + id);
        };


        return dataFactory;
    }])
    .factory('locationsModel', ['API', function (API) {

        var locationsModel = {};

        locationsModel.mapmarker = function ($scope) {

            $scope.location;

            return API.getSpace(id)
                .then(function (response) {
                    $scope.location = response.data;
                });

        }
        return locationsModel;

    }]);
