'use strict';

/**
 * @ngdoc service
 * @name networkhubApp.GoogleMapsInitializer
 * @description
 * # GoogleMapsInitializer
 * Factory in the networkhubApp.
 */
angular.module('networkhubApp')
    .factory('Initializer', function ($window, $q) {
        //Google's url for async maps initialization accepting callback function
        var asyncUrl = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyA_xmNKHOPZea9y06pNHmwoINT5SAZ8Rdw&libraries=places&callback=',
            mapsDefer = $q.defer();

        //Callback function - resolving promise after maps successfully loaded
        $window.googleMapsInitialized = mapsDefer.resolve; // removed ()

        //Async loader
        var asyncLoad = function (asyncUrl, callbackName) {
            var script = document.createElement('script');
            //script.type = 'text/javascript';
            script.src = asyncUrl + callbackName;
            document.body.appendChild(script);
        };
        //Start loading google maps
        asyncLoad(asyncUrl, 'googleMapsInitialized');

        //Usage: Initializer.mapsInitialized.then(callback)
        return {
            mapsInitialized: mapsDefer.promise
        };
    });
