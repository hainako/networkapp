'use strict';

/**
 * @ngdoc function
 * @name networkhubApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the networkhubApp
 */
angular.module('networkhubApp')
    .controller('SpaceCtrl',
        function ($scope, $http, API, ModalService, $routeParams, __env) {
            $scope.imageUrl = __env.apiImage;

            $scope.status;
            $scope.spaces;
            $scope.space;
            $scope.similars;
            var id = $routeParams.id;
          $scope.searchStr={};
           
           
            API.getSpace(id)
                .then(function (response) {
                    $scope.space = response.data.data;

                }, function (error) {
                    $scope.status = 'Unable to load Spaces data: ' + error.message;
                });
            $scope.date = new Date();
            var date = new Date().toISOString().substring(0, 10);
            var field = document.querySelector('#list_checkin');
            field.value = date;

            API.getSpaces().then(function (response) {

                $scope.spaces = response.data.data;

            })
            $scope.show = function () {
                ModalService.showModal({
                    templateUrl: 'views/reportmodal.html',
                    controller: "SpaceCtrl",
                }).then(function (modal) {
                    modal.element.modal();
                    modal.close.then(function (result) {
                        $scope.message = result;
                    });
                });
            };
           
            
            
            $scope.processForm = function () {
              
                $http({
                    method: 'POST',
                    url: 'https://test.thenetworkhub.net/cvm/webservices/listings/booking/',
                    data: {
                        firstname:$scope.searchStr.firstname, lastname:$scope.searchStr.lastname, email:$scope.searchStr.email, currentmember:$scope.selectedCountry, verified_by:$scope.searchStr.verifiedBy, visiting_date:field, guests:$scope.guests
                    },  // pass in data as strings
                    headers: {'Content-Type': 'application/json'}  // set the headers so angular passing info as form data (not request payload)
                })
                    .success(function (data, status) {
                        if (data.errors) {
                            // Showing errors.
                            $scope.errorName = 'Report not sent';
                            console.log(data);
                        } else {
                            $scope.msg = data['msg'];
                            console.log(data);
                        }


                    })
                    
            };
            

            $scope.showBook = function () {
                ModalService.showModal({
                    templateUrl: 'views/requestbooking.html',
                    controller: function($scope) {
                        $scope.visitingDate = document.querySelector('#list_checkin').value;
                        $scope.guests = document.querySelector('#number_of_guests').value;
                    }
                }).then(function (modal) {
                    console.log(modal);
                    $scope.modal = {firstname:$scope.searchStr.firstname, lastname:$scope.searchStr.lastname, email:$scope.searchStr.email, currentmember:$scope.selectedCountry, verified_by:$scope.searchStr.verifiedBy, visiting_date:date, guests:$scope.guests};
           console.log($scope.modal);
   
                    modal.element.modal();
                    modal.close.then(function (result) {
                        $scope.message = result;
                    });
                });
            };


            //$scope.searchStr = {};
            $scope.requestForm = function () {
                $http({
                    method: 'POST',
                    url: 'https://test.thenetworkhub.net/cvm/webservices/listings/report/',
                    data: {
                        firstname: $scope.searchStr.firstname,
                        lastname: $scope.searchStr.lastname,
                        email: $scope.searchStr.email,
                        currentmember: $scope.selectedSpace
                    },  // pass in data as strings
                    headers: {'Content-Type': 'application/json'}  // set the headers so angular passing info as form data (not request payload)
                })
                    .success(function (data, status) {

                        if (data.errors) {
                            // Showing errors.
                            $scope.errorName = 'Report not sent';
                            console.log(data);
                        } else {
                            $scope.msg = data['msg'];

                        }
                        console.log(data);

                    })
            };

        })
