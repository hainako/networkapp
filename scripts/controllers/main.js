'use strict';

/**
 * @ngdoc function
 * @name networkhubApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the networkhubApp
 */

angular.module('networkhubApp')
    .controller('MainCtrl', function ($scope, API, Initializer) {
        const style = [{
            "featureType": "water",
            "elementType": "geometry.fill",
            "stylers": [{"color": "#d3d3d3"}]
        }, {
            "featureType": "transit",
            "stylers": [{"color": "#808080"}, {"visibility": "off"}]
        }, {
            "featureType": "road.highway",
            "elementType": "geometry.stroke",
            "stylers": [{"visibility": "on"}, {"color": "#b3b3b3"}]
        }, {
            "featureType": "road.highway",
            "elementType": "geometry.fill",
            "stylers": [{"color": "#ffffff"}]
        }, {
            "featureType": "road.local",
            "elementType": "geometry.fill",
            "stylers": [{"visibility": "on"}, {"color": "#ffffff"}, {"weight": 1.8}]
        }, {
            "featureType": "road.local",
            "elementType": "geometry.stroke",
            "stylers": [{"color": "#d7d7d7"}]
        }, {
            "featureType": "poi",
            "elementType": "geometry.fill",
            "stylers": [{"visibility": "on"}, {"color": "#ebebeb"}]
        }, {
            "featureType": "administrative",
            "elementType": "geometry",
            "stylers": [{"color": "#a7a7a7"}]
        }, {
            "featureType": "road.arterial",
            "elementType": "geometry.fill",
            "stylers": [{"color": "#ffffff"}]
        }, {
            "featureType": "road.arterial",
            "elementType": "geometry.fill",
            "stylers": [{"color": "#ffffff"}]
        }, {
            "featureType": "landscape",
            "elementType": "geometry.fill",
            "stylers": [{"visibility": "on"}, {"color": "#efefef"}]
        }, {
            "featureType": "road",
            "elementType": "labels.text.fill",
            "stylers": [{"color": "#696969"}]
        }, {
            "featureType": "administrative",
            "elementType": "labels.text.fill",
            "stylers": [{"visibility": "on"}, {"color": "#737373"}]
        }, {
            "featureType": "poi",
            "elementType": "labels.icon",
            "stylers": [{"visibility": "off"}]
        }, {
            "featureType": "poi",
            "elementType": "labels",
            "stylers": [{"visibility": "off"}]
        }, {
            "featureType": "road.arterial",
            "elementType": "geometry.stroke",
            "stylers": [{"color": "#d6d6d6"}]
        }, {
            "featureType": "road",
            "elementType": "labels.icon",
            "stylers": [{"visibility": "off"}]
        }, {}, {"featureType": "poi", "elementType": "geometry.fill", "stylers": [{"color": "#dadada"}]}]


        API.getSpaces().then(function (response) {
            $scope.values = response.data.data;
            var initializeMap = function() {
            var values = response.data.data;
            var latLng = new google.maps.LatLng($scope.values[0].latitude, $scope.values[0].longitude);
            var markericon = {icon: 'images/map.png'};


            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 4,
                center: latLng,
                styles: style,
                scrollwheel: false
            });


            var infowindow = new google.maps.InfoWindow();

            var marker, i;

            for (i = 0; i < values.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(values[i]['latitude'], values[i]['longitude']),
                    icon: markericon.icon,
                    map: map
                });
                var content = '<div class="listing listing-map-popover">' +
                    '<div class="panel-image listing-img">' +


                    '<div class="listing-img-container media-cover text-center"><img id="marker_image_'+ values[i]['id'] +'" ' +
                    'rooms_image="" alt="" class="img-responsive-height" data-current="0" ' +
                    'src="https://test.thenetworkhub.net/cvm/uploads/' + values[i]['space_photo'] + '"></div>' +
                    '</div>' +
                    '<a href="#spaces/'+ values[i]['property_id'] +'" ><h3 id="firstHeading" class="firstHeading">' + values[i]['company'] + '</h3></a>' +
                    '<p>' + values[i]['city'] + ' ' + values[i]['address'] + '</p>' +
                    '<p> Website: <a href="' + values[i]['website'] + '">' + values[i]['website'] + '</a></p>' +

                    '</div>';


                google.maps.event.addListener(marker, 'click', (function (marker, content, infowindow) {
                    return function () {
                        infowindow.setContent(content);
                        infowindow.open(map, marker);

                    };

                })(marker, content, infowindow));
                google.maps.event.addListener(map, "click", function () {
                    if (infowindow) {
                        infowindow.close();
                    }
                });
            }
        }
                    Initializer.mapsInitialized.then(function () {
                                    // Promised resolved
                                    initializeMap();
                                })   

        })

    });

 