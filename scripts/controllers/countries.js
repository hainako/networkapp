'use strict';

/**
 * @ngdoc function
 * @name networkhubApp.controller:CountriesCtrl
 * @description
 * # CountriesCtrl
 * Controller of the networkhubApp
 */
angular.module('networkhubApp')
    .controller('CountriesCtrl', ['$scope', 'API',
        function ($scope, API) {

            $scope.countries;
            getCountries();

            function getCountries() {
                API.getCountries()
                    .then(function (response) {
                        $scope.countries = response.data;
                    }, function (error) {
                        $scope.status = 'Unable to load customer data: ' + error.message;
                    });
            }
        }]);
