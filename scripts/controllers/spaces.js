'use strict';

/**
 * @ngdoc function
 * @name networkhubApp.controller:SpacesCtrl
 * @description
 * # SpacesCtrl
 * Controller of the networkhubApp
 */
angular.module('networkhubApp')
  .controller('SpacesCtrl',
        function ($scope, API, $routeParams, __env) {
    $scope.imageUrl = __env.apiImage;
     $scope.status;     
     $scope.spaces;
     
     var name = $routeParams.slug;
  
        API.getCountrySpaces(name)
            .then(function (response) {
                $scope.spaces = response.data;
            }, function (error) {
                $scope.status = 'Unable to load Spaces data: ' + error.message;
            });
    
    });

