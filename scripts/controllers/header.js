'use strict';

/**
 * @ngdoc function
 * @name networkhubApp.controller:HeaderCtrl
 * @description
 * # HeaderCtrl
 * Controller of the networkhubApp
 */
angular.module('networkhubApp')
    .controller('headerCtrl', function ($scope, API, ModalService, $http, $window, $location) {
        var style = [{
            "featureType": "water",
            "elementType": "geometry.fill",
            "stylers": [{"color": "#d3d3d3"}]
        }, {
            "featureType": "transit",
            "stylers": [{"color": "#808080"}, {"visibility": "off"}]
        }, {
            "featureType": "road.highway",
            "elementType": "geometry.stroke",
            "stylers": [{"visibility": "on"}, {"color": "#b3b3b3"}]
        }, {
            "featureType": "road.highway",
            "elementType": "geometry.fill",
            "stylers": [{"color": "#ffffff"}]
        }, {
            "featureType": "road.local",
            "elementType": "geometry.fill",
            "stylers": [{"visibility": "on"}, {"color": "#ffffff"}, {"weight": 1.8}]
        }, {
            "featureType": "road.local",
            "elementType": "geometry.stroke",
            "stylers": [{"color": "#d7d7d7"}]
        }, {
            "featureType": "poi",
            "elementType": "geometry.fill",
            "stylers": [{"visibility": "on"}, {"color": "#ebebeb"}]
        }, {
            "featureType": "administrative",
            "elementType": "geometry",
            "stylers": [{"color": "#a7a7a7"}]
        }, {
            "featureType": "road.arterial",
            "elementType": "geometry.fill",
            "stylers": [{"color": "#ffffff"}]
        }, {
            "featureType": "road.arterial",
            "elementType": "geometry.fill",
            "stylers": [{"color": "#ffffff"}]
        }, {
            "featureType": "landscape",
            "elementType": "geometry.fill",
            "stylers": [{"visibility": "on"}, {"color": "#efefef"}]
        }, {
            "featureType": "road",
            "elementType": "labels.text.fill",
            "stylers": [{"color": "#696969"}]
        }, {
            "featureType": "administrative",
            "elementType": "labels.text.fill",
            "stylers": [{"visibility": "on"}, {"color": "#737373"}]
        }, {
            "featureType": "poi",
            "elementType": "labels.icon",
            "stylers": [{"visibility": "off"}]
        }, {
            "featureType": "poi",
            "elementType": "labels",
            "stylers": [{"visibility": "off"}]
        }, {
            "featureType": "road.arterial",
            "elementType": "geometry.stroke",
            "stylers": [{"color": "#d6d6d6"}]
        }, {
            "featureType": "road",
            "elementType": "labels.icon",
            "stylers": [{"visibility": "off"}]
        }, {}, {"featureType": "poi", "elementType": "geometry.fill", "stylers": [{"color": "#dadada"}]}]


        $scope.geolocation = function () {

            var initializeMap = function() {
            navigator.geolocation.getCurrentPosition(function (position) {
                var locat = new google.maps.LatLng(34.0522, -118.2437);
                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 4,
                    center: locat,
                    styles: style
                });
                var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                var markericon = {icon: 'images/map.png'};
                // Create a marker and center map on user location
                var marker = new google.maps.Marker({
                    position: pos,
                    icon: markericon.icon,
                    map: map
                });

                map.setCenter(pos);
            });
           }
                    Initializer.mapsInitialized.then(function () {
                                    // Promised resolved
                                    initializeMap();
                                })    

        }

        API.getSpaces().then(function (response) {

            $scope.spaceSearch = response.data.data;

        });

       $scope.Data = {};
         $scope.Data.check = {
            value : true
            };
    $scope.file = null;
    $scope.fileModel = '';
$scope.$watch('file', function (newVal) {
        if (newVal)
          console.log(newVal);
      })
                  
   
       
             
            // NOW UPLOAD THE FILES.
        $scope.addForm = function (file) {
              
                var formdata = new FormData();
       

            
            
            var request = {
                    method: 'POST',
                    url: 'https://test.thenetworkhub.net/cvm/webservices/listings/add',
                    data: {firstname: $scope.Data.firstname,lastname:$scope.Data.lastname,company:$scope.Data.company,address:$scope.Data.address,free_days:$scope.Data.free_days,terms:$scope.Data.check.value, website:$scope.Data.website, email:$scope.Data.email, phone:$scope.Data.phone, logo:$scope.fileModel.name,space_photo:$scope.file.name},
                   headers: {'Content-Type': 'application/json'}
     
                         
                };

                // SEND THE FILES.
                $http(request)
                   .success(function (data, status) {
                        if (data.errors) {
                            // Showing errors.
                            console.log(data);
                            $scope.errorName = data['msg'];

                        } else {
                            $scope.msg = 'Space added sent';
                           
                        }


                    })
                    console.log(request.data);
            }
        $scope.showAdd = function () {
            ModalService.showModal({
                templateUrl: 'views/mainheadermodal.html',
                controller: "headerCtrl"
            }).then(function (modal) {
                // only called on success...
                modal.element.modal();
            }).catch(function (error) {
                // error contains a detailed error message.
                console.log(error);
            });
        };


    });



 



